# a bot to post server info to misskey
using JobSchedulers

"""
    PostCpuStat

Post server cpu statistics to Misskey.
"""
module PostCpuStat

using Misskey
using Dates

# read token fron `token`
token = readchomp(joinpath(@__DIR__, "..", "token"))
@info token

# read server url from `server`
server = readchomp(joinpath(@__DIR__, "..", "server"))
@info server

function post_cpustat()
    # io = IOBuffer()
    # Base.Sys.cpu_summary(io)
    # cpu_stat = String(take!(io))
    # @info cpu_stat
    cpuinfo = Base.Sys.cpu_info()
    cpu_stat = """
        $(cpuinfo[1].model)
        """ * join(
        Base.Sys.cpu_info() |>
        enumerate .|>
        (x -> "#$(x[1]) $(x[2].speed) MHz\n")
    )
    stat_date = now()

    @info cpu_stat
    @info now()
    create_param = Misskey.Notes.create_params(Text="**CPU stats** on $(stat_date):\n" * cpu_stat, i=token)
    # since methods only accepts `String`
    Misskey.Notes.create(String(server), create_param)
end

end # PostCpuStat

# create job
job_post_cpustat = Job(
    PostCpuStat.post_cpustat,
    name = "Post CPU statistics to misskey.",
    user = "me",
    ncpu = 1,
    mem = 1KB,
    schedule_time = Second(3),
    wall_time = Minute(3),
    cron = Cron(0, "*/30", *, *, *, *), # every 30 minutes
)

submit!(job_post_cpustat)

